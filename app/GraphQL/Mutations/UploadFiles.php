<?php

namespace App\GraphQL\Mutations;

class UploadFiles
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $logoImageFile = $args['logoImageFile'];

        $logoImageFile->storePublicly('uploads');
        // TODO implement the resolver
    }
}
