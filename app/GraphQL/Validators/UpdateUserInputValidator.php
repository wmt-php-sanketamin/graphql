<?php

namespace App\GraphQL\Validators;

use Nuwave\Lighthouse\Validation\Validator;

class UpdateUserInputValidator extends Validator
{
    /**
     * Return the validation rules.
     *
     * @return array<string, array<mixed>>
     */
    public function rules(): array
    {
        return [
            // TODO Add your validation rules
            'id' => [
                'required'
            ],
            'name' => [
                'sometimes',
                Rule::unique('users', 'name')->ignore($this->arg('id'), 'id'),
            ],
        ];
    }
    public function messages(): array
    {
        return [
            'name.unique' => 'The chosen username is not available',
        ];
    }
}
